# Trasher Bot

## Instructions

Structure is based on [this article](https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure). Source code can be found in the `src` folder, and tests in the `tests` folder.

To install the package, simply execute

```bash
➤ pip install -r requirements_dev.txt
```

We use `tox` for the tests. This ensure a clear separation between the development environment and the test environment.
To launch the tests, run the `tox` command:

```bash
➤ tox
```

It first starts with a bunch of checks (`flask8` and others) and then launch the tests using python 3.

## Packaging

A Dockerfile is provided to build an image ready to be deployed, see `docker/Dockerfile`. You can build the image using `make`:

```bash
➤ make docker
```

This will create a new image named ``.

# PyPi: Docker container for PyPi server

## Run

### On Windows with Docker

```cmd
λ docker build -t yoyonel/forcity_trasherbot:0.1.0 -f docker\Dockerfile  .
...
Successfully built 14c63bf8a20f
Successfully tagged yoyonel/forcity_trasherbot:0.1.0

[...]\forcity_trasherbot (master -> origin)
λ docker run -it --rm -v %cd%/data:/data yoyonel/forcity_trasherbot:0.1.0 /data/config_00.json
2018-12-01 15:33:03,408 - forcity.trasherbot.app - INFO - application version: 0.1.0
2018-12-01 15:33:03,412 - forcity.trasherbot.model.trash - INFO - init game: trash pop, remaining 1
2018-12-01 15:33:03,412 - forcity.trasherbot.model.trash - INFO - init game: trash pop, remaining 2
2018-12-01 15:33:03,412 - forcity.trasherbot.model.trash - INFO - init game: trash pop, remaining 3
2018-12-01 15:33:03,413 - forcity.trasherbot.app - INFO - Round 2/50: trash collected, remaining 2
2018-12-01 15:33:03,413 - forcity.trasherbot.app - INFO - Round 4/50: trash collected, remaining 1
2018-12-01 15:33:03,413 - forcity.trasherbot.app - INFO - Round 5/50: trash collected, remaining 0
2018-12-01 15:33:03,414 - forcity.trasherbot.app - INFO - success. Collected all trashs in 5 rounds
```

## Screencast (Asciinema)

[![asciicast]()

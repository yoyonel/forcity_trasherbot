"""

"""
from dataclasses import dataclass
import logging
import random
from typing import Optional
#
from forcity.trasherbot.model.bot import Bot
from forcity.trasherbot.model.position import Position
from forcity.trasherbot.model.map import Map
from forcity.trasherbot.model.trash import Trash, Trashes


logger = logging.getLogger(__name__)


@dataclass
class Board:
    # `Board` objects
    field: Map
    trashes: Trashes
    bot: Bot

    def __post_init__(self):
        for trash in self.trashes:
            self.field.add_trash(trash)

    @classmethod
    def from_json(cls, json_config: dict) -> 'Board':
        """
        pre-requisite: json_config is a valide json datas configuration (see `load_config:validate_config()`)

        :param json_config:
        :return:
        """
        return cls(
            field=Map.from_json(json_config['map']),
            bot=Bot.load(json_config['bot']),
            trashes=Trashes.from_json(json_config['trash']),
        )

    @property
    def nb_trashes_remaining(self):
        return len(self.trashes)

    def gen_propagate_trashes(self):
        # on parcourt la liste des trashes qui se propagent
        for trash_propaged in self.trashes.propage():
            # on recupère pour chaque trash à propager la liste des directions possibles de propagation
            # on en choisit une (au hasard)
            try:
                trash_position_to_propagate = random.choice(list(self.field.free_neighbors(trash_propaged.position)))
            except IndexError:
                # no propagation from this trash because no available position to propagate
                continue

            trash_to_propagate = Trash(position=trash_position_to_propagate)
            self.field.add_trash(trash_to_propagate)
            self.trashes.add(trash_to_propagate)
            yield trash_to_propagate

    def move_bot(self, target: Position, collect_trash: bool = True) -> bool:
        """

        :param target:
        :param collect_trash:
        :return:
        """
        trash_collected = False
        self.bot.move(target, self.field.max_range)
        if collect_trash:
            trash_to_collect = self.field.get_trash(self.bot.position)
            if trash_to_collect is not None:
                self.trashes.remove(trash_to_collect)
                self.field.remove_trash(trash_to_collect)
                trash_collected = True

        return trash_collected

    def find_closest_trash(self) -> Optional[Trash]:
        try:
            # TODO: Need to optimize ! Inefficient ! (but very simple :p)
            # sort ALL trashes by distance from the bot and select (on of/the first) the closest
            return sorted(
                self.trashes,
                key=lambda trash: (trash.position - self.bot.position).distance()
            )[0]
        except IndexError:
            return None
